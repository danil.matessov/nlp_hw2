import torch
from transformers import GPT2Tokenizer, GPT2LMHeadModel
from transformers import AutoTokenizer, T5ForConditionalGeneration

class Bot():
    def __init__(self):
        self.device = 'cpu'
        if torch.cuda.is_available():
            self.device = 'cuda'
        print('Running on', self.device)
        
        self.generator = GPT2LMHeadModel.from_pretrained('gpt2').to(self.device)
        self.g_tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
        
        self.styler = T5ForConditionalGeneration.from_pretrained('model/t5_small_train_3000').to(self.device)
        self.s_tokenizer = AutoTokenizer.from_pretrained('google-t5/t5-small')

    def paraphrase(self, text, n=None, max_length='auto', temperature=50.0, beams=10):
        texts = [text] if isinstance(text, str) else text
        inputs = self.s_tokenizer(texts, return_tensors='pt', padding=True)['input_ids'].to(self.device)
        if max_length == 'auto':
            max_length = int(inputs.shape[1] * 1.2) + 10
        result = self.styler.generate(
            inputs, 
            num_return_sequences=n or 1, 
            do_sample=False, 
            #temperature=temperature, 
            repetition_penalty=3.0, 
            max_length=max_length,
            bad_words_ids=[[2]],  # unk
            num_beams=beams,
        )
        texts = [self.s_tokenizer.decode(r, skip_special_tokens=True) for r in result]
        if not n and isinstance(text, str):
            return texts[0]
        return texts

    def get_response(self, input_text):
        # Convert our text into numbers the model understands
        input_ids = self.g_tokenizer.encode(input_text, return_tensors="pt").to(self.device)
    
        # Ask the model to craft a suitable response
        output = self.generator.generate(input_ids, max_length=70,
         num_return_sequences=2,
         pad_token_id=self.g_tokenizer.eos_token_id,
         top_k=50,
         top_p=0.95,
         do_sample=True
        )
    
        # Change those numbers back into words
        response = self.g_tokenizer.decode(output[0], skip_special_tokens=True)
        response = response[len(input_text):].replace("\n", " ")

        return self.paraphrase(response)


