from flask import Flask
from bot import Bot


bot = Bot()
app = Flask(__name__)

@app.route("/<query>")
def house(query):
    return bot.get_response(query)

if __name__ == '__main__':
    app.run(port=5000, debug=True)